def intInput(str=...):
  """[입력값을 받아 정수형으로 리턴한다.]

  Args:
      str ([string]): [입력 요청 문구]

  Returns:
      int: [정수형 리턴]
  """
  return int(input(str))

def floatInput(str=...):
  """[입력값을 받아 실수형으로 리턴한다.]

  Args:
      str ([string]): [입력 요청 문구]

  Returns: 
    float
  """
  return float(input(str))

if __name__ =="__main__":
  import sys
  intInput("test key: ")