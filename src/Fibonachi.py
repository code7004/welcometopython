db = {0:0, 1:1}

def fib(n):
  if n not in db:
    db[n] = fib(n-1) + fib(n-2)
                
  return db[n]

print(db, "end = ", fib(0))
print(db, "end = ", fib(20))
