print("한글테스트")


datas = []

datas.append(["I","am","LIST", "I","am","LIST"])
datas.append(("I","am","TUPLE"))
datas[1] += tuple(["x" * 10])


for i in range(len(datas)):
  for j in range(len(datas[i])):
    print(type(datas[i]), datas[i][j],end = " ")
  print()

print(type(datas), datas)
print(datas[0][1:4])


set = {"I", "am", "SET","I", "am", "SET"}
set.add("I")
set.add("YOU")
set.update("I", "YOU")
for i in set: print(i,end=" ")
for i in sorted(set): print(i,end=" ")
print()

mDic = {}
mDic["a"] = "A"
mDic["b"] = "B"
mDic["c"] = "C"

print(mDic)
for d in mDic.items() : print(d, end=" ")
print()
for d in mDic : print(d, end=" ")
print()
for d in mDic : print(d, mDic[d], end=" ")
print()
for d in mDic.values() : print(d, end=" ")
print()
