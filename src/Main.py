rows = 20
cols = 20

rawList=[]

def printList(list):
  for row in range(len(list)):
    for col in range(len(list[row])):
      print(list[row][col], end=" ")
    print()

def initList(list):
  for row in range(len(list)):
    for col in range(len(list[row])):
      if (row + col) % 2 == 0 : list[row][col] = 0
      else : list[row][col] = 1


for row in range(rows): rawList += [[0] * cols]
# rawList += [[0] * cols]

initList(rawList)

printList(rawList)
