from modules.MyInput import *
from pathlib import Path
import os
import csv

#####################################################################################
# const
#####################################################################################
ROOT_PATH = Path(__file__).parent.parent
FILE_PATH = str(ROOT_PATH) + '\\resources\\address.csv'

#####################################################################################
# functions
#####################################################################################
def drawMembers():
  for mem in range(len(members)): print(str(mem + 1) + ". " , members[mem])
  
def drawMenus():
  print("-" * 50)
  print("메뉴를 입력하세요.")
  print("-" * 50)
  for m in range(len(menus)):
    print(str(m + 1) + ". ", menus[m])
  print("-" * 50)

def saveFile():
  with open(FILE_PATH, "w+", encoding='utf-8', newline="") as file:
    wr = csv.writer(file)
    for mem in members: wr.writerow([mem])
  print("저장되었습니다.")
    
def loadFile():
  if os.path.isfile(FILE_PATH) == False : return []
  
  temp = []
  f = open(FILE_PATH, "r", encoding='utf-8', newline="")
  rdr = csv.reader(f)
  for line in rdr:
    temp.append(line[0])
  f.close()
  return temp

#####################################################################################
# global variables
#####################################################################################
menus = ["친구 리스트 출력", "친구 추가", "친구 삭제", "이름변경","저장하기", "종료"]
members = loadFile()
menuIdx = 0


#####################################################################################
# main logic
#####################################################################################
os.system('cls')#os.system("clear")
drawMembers()
drawMenus()  
menuIdx = intInput("메뉴를 입력하세요: ") - 1  

while menuIdx != 9:
  print(menus[menuIdx] + " >> ")
  if menuIdx == 0 :    
    drawMembers()
  elif menuIdx == 1:
    add = input("추가할 이름을 입력 하세요: ")
    members.append(add)
  elif menuIdx == 2:
    delMem = input("삭제할 이름을 입력 하세요: ")
    if delMem in members:
      idx = members.index(delMem)
      del members[idx]
    else:
      print("친구를 찾을수 없습니다")
  elif menuIdx == 3:
    idx = intInput("변경할 번호를 입력 하세요: ")
    if idx in range(len(members)): 
      members[idx - 1] = input("이름을 입력하세요: ")    
    else:
      print("친구를 찾을수 없습니다")
  elif menuIdx == 4:
    saveFile()
  
  
  drawMenus()  
  menuIdx = intInput("메뉴를 입력하세요: ") - 1  
  
  os.system('cls')#os.system("clear")
